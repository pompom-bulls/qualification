#include "gladiator.h"
#include <cmath>
#undef abs

class Vector2 {
public:
    Vector2() : _x(0.), _y(0.) {}
    Vector2(float x, float y) : _x(x), _y(y) {}

    float norm1() const { return std::abs(_x) + std::abs(_y); }
    float norm2() const { return std::sqrt(_x*_x+_y*_y); }
    void normalize() { _x/=norm2(); _y/=norm2(); }
    Vector2 normalized() const { Vector2 out=*this; out.normalize(); return out; }

    Vector2 operator-(const Vector2& other) const { return {_x-other._x, _y-other._y}; }
    Vector2 operator+(const Vector2& other) const { return {_x+other._x, _y+other._y}; }
    Vector2 operator*(float f) const { return {_x*f, _y*f}; }

    bool operator==(const Vector2& other) const { return std::abs(_x-other._x) < 1e-5 && std::abs(_y-other._y)<1e-5; }
    bool operator!=(const Vector2& other) const { return !(*this == other); }

    float x() const { return _x;}
    float y() const { return _y;}

    float dot(const Vector2& other) const { return _x*other._x + _y*other._y; }
    float cross(const Vector2& other) const { return _x*other._y - _y*other._x; }
    float angle(const Vector2& m) const { return std::atan2(cross(m), dot(m)); }
    float angle() const { return std::atan2(_y, _x); }
private:
    float _x, _y;
};

Gladiator* gladiator;

void reset() {
}

inline float moduloPi(float a) // return angle in [-pi; pi]
{
    return (a < 0.0) ? (std::fmod(a - M_PI, 2*M_PI) + M_PI) : (std::fmod(a + M_PI, 2*M_PI) - M_PI);
}

inline float   modulo2Pi(float a) {
    if (a < - M_PI)
        a += 2 * M_PI;
    if (a >= M_PI)
        a -= 2 * M_PI;
    return (a);
}

inline bool aim(Gladiator* gladiator, const Vector2& target, bool showLogs)
{
    constexpr float ANGLE_REACHED_THRESHOLD = 0.1;
    constexpr float POS_REACHED_THRESHOLD = 0.05;

    auto posRaw = gladiator->robot->getData().position;
    Vector2 pos{posRaw.x, posRaw.y};

    Vector2 posError = target - pos;

    float targetAngle = posError.angle();
    float angleError = moduloPi(targetAngle - posRaw.a);

    bool targetReached = false;
    float leftCommand = 0.f;
    float rightCommand = 0.f;

    if (posError.norm2() < POS_REACHED_THRESHOLD) //
    {
        targetReached = true;
    } 
    else if (std::abs(angleError) > ANGLE_REACHED_THRESHOLD)
    {
        float factor = 0.2;
        if (angleError < 0)
            factor = - factor;
        rightCommand = factor;
        leftCommand = -factor;
    }
    else {
        float factor = 0.5;
        rightCommand = factor;//+angleError*0.1  => terme optionel, "pseudo correction angulaire";
        leftCommand = factor;//-angleError*0.1   => terme optionel, "pseudo correction angulaire";
    }

    gladiator->control->setWheelSpeed(WheelAxis::LEFT, leftCommand);
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, rightCommand);

    if (showLogs || targetReached)
    {
        gladiator->log("ta %f, ca %f, ea %f, tx %f cx %f ex %f ty %f cy %f ey %f", targetAngle, posRaw.a, angleError, target.x(), pos.x(), posError.x(), target.y(), pos.y(), posError.y());
    }

    return targetReached;
}

int XYId(int x, int y) {
    return(x + y * 14);
}

int idI(int id) {
    return(id / 14);
}

int idJ(int id) {
    return(id % 14);
}

float   getX(int i) {
    return (i + 0.5 * gladiator->maze->getSquareSize());
}

float   getY(int j) {
    return (j + 0.5 * gladiator->maze->getSquareSize());
}

MazeSquare*  whosNeighbor(int dir) {
    switch (dir) {
        case 0:
            return(gladiator->maze->getNearestSquare().northSquare);
            break;
        case 1:
            return(gladiator->maze->getNearestSquare().eastSquare);
            break;
        case 2:
            return(gladiator->maze->getNearestSquare().southSquare);
            break;
        case 3:
            return(gladiator->maze->getNearestSquare().westSquare);
            break;
    }
    return (NULL);
}

int   updateWallMove(unsigned startingTime) {
        return((millis() - startingTime) / 1000);
};
unsigned startingTime = 0;

MazeSquare*  findNextAvailableNeighbor(int dir) {
    MazeSquare*  nextVisitedNeighbor;

    for (int i = 0; i < 4; ++i) {
        nextVisitedNeighbor = whosNeighbor(dir);
        if (nextVisitedNeighbor)
            return (nextVisitedNeighbor);
    }
    return (NULL);
}

void setup() {
    //instanciation de l'objet gladiator
    gladiator = new Gladiator();
    //enregistrement de la fonction de reset qui s'éxecute à chaque fois avant qu'une partie commence
    gladiator->game->onReset(&reset);
}

void loop() {
    if (gladiator->game->isStarted())
    {
        if (!startingTime)
            startingTime = millis();
        
        static unsigned i = 0;          //basic
        bool showLogs = (i%50 == 0);    //basic
        
        Position    position;
        int         wallMove;
        float       angle;
        int         orientationSquare = 0;
        MazeSquare* nextVisitedSquare;
        Vector2     targetCoord{0., 0.};
        
        //position = gladiator->robot->getData().position;
        wallMove = updateWallMove(startingTime);
        angle = position.a;
        orientationSquare = modulo2Pi(angle) / (M_PI / 2);
        nextVisitedSquare = findNextAvailableNeighbor(orientationSquare);
        targetCoord = Vector2(getX(nextVisitedSquare->i), getY(nextVisitedSquare->j));
        //if (aim(gladiator, {1.5, 1.5}, showLogs))       //basic
        if (aim(gladiator, targetCoord, showLogs))       //basic
        {
            gladiator->log("target atteinte !");        //basic
        }
        i++;                                            //basic
    }
    delay(10); // boucle à 100Hz
}