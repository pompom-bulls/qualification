#include "gladiator.h"
#include <iostream>

Gladiator* gladiator;

float  dist(Position position0, Position position1) {
    float a = (position1.x - position0.x);
    float b = (position1.y - position0.y);

    a = a * a;
    b = b * b;
    return(sqrt(a + b));
}

float   distTarget(float rX, float rY, float tX, float tY) {
    float a = (tX - rX);
    float b = (tY - rY);

    a = a * a;
    b = b * b;
    return(sqrt(a + b));
}

float  modulo2Pi(float a) {
    if (a < - M_PI)
        a += 2 * M_PI;
    if (a >= M_PI)
        a -= 2 * M_PI;
    return (a);
}

float  moduloPi(float a) {
    if (a < 0)
        a += M_PI;
    if (a >= M_PI)
        a -= M_PI;
    return (a);
}

int sign(float f) {
    if (f < 0)
        return (-1);
    else
        return (1);
}

float   getX(int i) {
    return ((i + 0.5) * gladiator->maze->getSquareSize());
}

float   getY(int j) {
    return ((j + 0.5) * gladiator->maze->getSquareSize());
}

void    turn(float angle, float speed) {
    float   a0 = gladiator->robot->getData().position.a;
    float   a = gladiator->robot->getData().position.a;
    while (sign(angle) * modulo2Pi(a - a0) < sign(angle) * angle) {
        gladiator->control->setWheelSpeed(WheelAxis::RIGHT, (angle >= 0) * speed); //controle de la roue droite
        gladiator->control->setWheelSpeed(WheelAxis::LEFT, (angle < 0) * speed); //control de la roue gauche
        delay(100);
        a = gladiator->robot->getData().position.a;
    }
}

void    goTime(float speed, int time) {
    gladiator->control->setWheelSpeed(WheelAxis::RIGHT, speed); //controle de la roue droite
    gladiator->control->setWheelSpeed(WheelAxis::LEFT, speed); //control de la roue gauche
    delay(time);
}

void    goDist(float speed, float d) {
    Position position0 = gladiator->robot->getData().position;
    Position position = gladiator->robot->getData().position;
    while (dist(position0, position) < d) {
        gladiator->control->setWheelSpeed(WheelAxis::RIGHT, speed); //controle de la roue droite
        gladiator->control->setWheelSpeed(WheelAxis::LEFT, speed); //control de la roue gauche
        delay(100);
        position = gladiator->robot->getData().position;
    }
}

bool    hasReachedOppositeSide(int dir, float speed) {
    int targetVal = 0;

    if (speed > 0)
        targetVal = 13;
    else
        targetVal = 0;
    if (dir == 1)
        return(gladiator->robot->getData().cposition.x == targetVal);
    else
        return(gladiator->robot->getData().cposition.y == targetVal);
}

void reset();
void setup() {
    //instanciation de l'objet gladiator
    gladiator = new Gladiator();
    //enregistrement de la fonction de reset qui s'éxecute à chaque fois avant qu'une partie commence
    gladiator->game->onReset(&reset);
    gladiator->game->enableFreeMode(RemoteMode::OFF);
}

void reset() {
    //fonction de reset:
    //initialisation de toutes vos variables avant le début d'un match
}

void loop() {
    if (gladiator->game->isStarted()) { //tester si un match à déjà commencer
        //code de votre stratégie :
        //Obtenir les données du robot
        //RobotData data = gladiator->robot->getData();
        // position du robot (filtré)
        //Position position0 = data.position;
        //Position position1 = data.position;
        //goTime (0.3, 5000);
        //goTime (0.6, 2000);
        //goDist (-0.3, 1);
        //turn (M_PI / 2, 0.3);
        //goTime (0.3, 1000);
        //turn (-M_PI / 2, 0.3);
        //dijkstraTableInit(dijkstraTable);
        //dijkstraSearch(gladiator->maze->getNearestSquare, gladiator->maze->getSquare(13,13), dijkstraTable);
        //route = readDijkstraPath(gladiator->maze->getNearestSquare, gladiator->maze->getSquare(13,13), dijkstraTable, 0);
        
        //float targetAngle;
        //targetSquare = gladiator->maze->getSquare(r.x + 1, r.y);
        //targetSquare = route[1];
        //targetAngle = moduloPi(atan2(getY(targetSquare.j)-r.y, getX(targetSquare.i)-r.x)-r.a);
        //turn(targetAngle, 0.6);

        MazeSquare  targetSquare;
        MazeSquare  startSquare;
        
        Position r0;
        int dir;
        float speed;
        bool    reachOppositeSide;

        dir = 1;
        r0 = gladiator->robot->getData().cposition;
        startSquare = gladiator->maze->getNearestSquare();
        if (startSquare.i == 0 || startSquare.i == 13)
            dir = 1;
        else
            dir = 2;
        if (startSquare.i == 0 || startSquare.j == 0)
            speed = 0.3;
        else
            speed = -0.3;
        
        while (!hasReachedOppositeSide) {
            goDist(speed, 0.5);
            delay(50);
        }
    }

    //La consigne en vitesse est forcée à 0 lorsque aucun match n'a débuté.
}